FROM kwart/alpine-ext
MAINTAINER Danjelo Morgaux <dan@interwebdev.com>

ENV BUILD_PACKAGES nodejs 

# Update and install all of the required packages.
# At the end, remove the apk cache
RUN apk update && \
    apk upgrade && \
    apk add $BUILD_PACKAGES && \
    rm -rf /var/cache/apk/*

#Init NodeJS
RUN mkdir /data
WORKDIR /data
COPY package.json ./package.json
RUN npm install
COPY . .

#Build Entrypoint
COPY docker-entrypoint.sh /

CMD []
ENTRYPOINT ["/docker-entrypoint.sh"]
